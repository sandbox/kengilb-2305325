-- SUMMARY --

The Stackla module provides a series of utility functions that utilize Stackla
API to communicate with the Stackla system. These functions will allow the
creation, update and deletion of both tags and filters.

-- REQUIREMENTS --

Requires an account with Stackla and a valid API key.

-- INSTALLATION --

* Install as usual, see http://www.drupal.org/documentation/install/modules-themes/modules-7 for more information.

-- CONFIGURATION --

* Configure user permissions in Administration >> People >> Permissions

  - Use Stackla
    All API requests require the base permission of using the Stackla API. An access exception error will be thrown
    and users will not be able to update tags and filters in Stackla without the proper permissions.

  - Administer Stackla
    The administrator page for configuring the API key requires this permission. Without it, a user
    will not be able to configure the API key or see an API key that has already been set.

* Configure the Stackla API key in Administration >> Configuration >> Services >> Stackla

  - The API key is needed to authenticate with Stackla in order to conduct any API calls.

-- TROUBLESHOOTING --

* If the menu does not display, check the following:

  - Are the 'Administer Stackla' permissions set for the proper roles?

* If tags or filters are not being updated in the Stackla account, check the following:

  - Is a valid stack name being sent with the API calls?
  - Is the API key properly set?

-- CONTACT --

Current maintainers:
* Ken Gilbert (kengib) - https://www.drupal.org/user/2754331

This project has been sponsored by:
* Achieve Internet
  http://www.achieveinternet.com
