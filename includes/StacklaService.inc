<?php
/**
 * @file
 * Implementation of the Stackla Service API calls.
 */

/**
 * Handles filter and tag administration with Stackla.
 */
class StacklaService implements iStacklaService {
  protected $serviceBaseUrl;
  protected $serviceApiKey;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->serviceBaseUrl = 'https://my.stackla.com/api';
    $this->serviceApiKey = variable_get('stackla_api_key', '');
  }

  //region Filters.
  /**
   * Gathers all filter data related to
   * the passed in stack.
   *
   * @param string $stack
   *   The stack short name to retrieve
   *   the filters for.
   *
   * @return array
   *   Returns an array of filters for
   *   the specified stack.
   */
  public function getAllfilters($stack) {
    $service_path = '/filters';
    $response = $this->SubmitRequest($service_path, $stack, 'GET', array());
    $response = $this->ParseServiceResponse($response, $service_path, array());

    if (!empty($response['data'])) {
      return $response['data'];
    }
    else {
      return array();
    }
  }

  /**
   * Gathers configuration settings about a filter
   * based on filter id. If there are external
   * products it will gather content that matches all
   * of the tags for this External Product Id.
   *
   * @param int $filter_id
   *   The id of the filter to retrieve content for.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $external_product_ids
   *   An array of external product ids.
   *   i.e. twitter id, facebook id, etc.
   *
   * @param array $external_product_types
   *   An array of external product types.
   *   i.e. twitter, facebook, etc.
   *
   * @return array
   *   Returns an array of information about the filter if
   *   successful, otherwise returns an empty array.
   */
  public function getFilter($filter_id, $stack, $external_product_ids = array(), $external_product_types = array()) {
    if (!is_numeric($filter_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla filter id.'), 'error');
      return array();
    }
    $data = array();
    if (!empty($external_product_ids)) {
      // This is not an error. This is the param name to send Stackla.
      $data['ext_product_id[]'] = $external_product_ids;
    }
    if (!empty($external_product_types)) {
      $list = implode(',', $external_product_types);
      $data['ext_product_id'] = $list;
    }

    $service_path = '/filters/' . $filter_id;
    $response = $this->SubmitRequest($service_path, $stack, 'GET', $data);
    $response = $this->ParseServiceResponse($response, $service_path, $data);

    if (!empty($response['data'])) {
      return $response['data'];
    }
    else {
      return array();
    }
  }

  /**
   * Gathers content that applies to a filter.
   *
   * @param int $filter_id
   *   The id of hte filter to retrieve content for.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'page' => int, // Page.
   *     'limit' => int, // Limit.
   *     'network' => string, // See the possible networks below.
   *     'scope' => string, // Either 'source', 'stackla', or 'all'.
   *     'm2v' => int, // Media type conversion.
   *     'search' => string, // Text search on the stack.
   *     // TODO: Figure out why stackla overloads this parameter and how to use it.
   *     'ext_product_id' => array(), // An array of external product ids.
   *     'ext_product_id => array(), // A formatted string of external product types comma separated. i.e. 'instagram,facebook,twitter'.
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return array
   *   Returns an array of content if successful, otherwise an
   *   empty array.
   */
  public function getFilterContent($filter_id, $stack, $settings = array()) {
    if (!is_numeric($filter_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla filter id.'), 'error');
      return array();
    }

    $service_path = '/filters/' . $filter_id . '/content';
    $response = $this->SubmitRequest($service_path, $stack, 'GET', $settings);
    $response = $this->ParseServiceResponse($response, $service_path, $settings);

    if (!empty($response['data'])) {
      return $response['data'];
    }
    else {
      return array();
    }
  }

  /**
   * Creates a new filter with optional settings.
   *
   * @param string $name
   *   The name of the new filter.
   *
   * @param string $stack
   *   The stack shortname to apply the filter to.
   *
   * @param array $settings
   *   array(
   *     'enabled' => 1, // 1, 2 or 3
   *     'system_filter' => TRUE, // Enable the system filter.
   *     'enabled_secondary' => TRUE, // Enable the secondary filter.
   *     'sort' => 'score_desc', // Either 'score_desc','source_created_at_desc','votes_desc','random'.
   *     'networks[]' => array(), // An array of available networks, see possible networks below.
   *     'tags[]' => array(), // An array of tag ids.
   *     'media[]' => array(), // An array of these options: 'image', 'text', 'video', 'html'.
   *     'terms[]' => array(), // An array of term ids.
   *     'geo_data_only' => TRUE, // Include geo data only.
   *     'display_name' => string, // Default: 'Discovery'
   *     'display_id' => string, // Default: 'category'
   *     'display_type' => string, // Default: 'dropdown'
   *     'filter_by_claimed' => string,
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return int|bool
   *   Returns the filter id if successful, otherwise FALSE.
   */
  public function createFilter($name, $stack, $settings = array()) {
    $settings['name'] = $name;

    // Hard code the only style available.
    $settings['style'] = 'dropdown';

    $service_path = '/filters';
    $response = $this->SubmitRequest($service_path, $stack, 'POST', $settings);
    $response = $this->ParseServiceResponse($response, $service_path, $settings);

    $success = (!empty($response['data']['id'])) ? $response['data']['id'] : FALSE;
    return $success;
  }

  /**
   * Updates an existing filter with the given settings.
   * Utilizes the PUT http request method.
   *
   * @param int $filter_id
   *   The Stackla id of the filter to modify.
   *
   * @param string $stack
   *   The stack name.
   *
   * @param array $settings
   *   array(
   *     'name' => string, // New name for the filter.
   *     'enabled' => 1, // 1, 2 or 3
   *     'system_filter' => TRUE, // Enable the system filter.
   *     'enabled_secondary' => TRUE, // Enable the secondary filter.
   *     'sort' => 'score_desc', // Either 'score_desc','source_created_at_desc','votes_desc','random'.
   *     'networks[]' => array(), // An array of available networks, see possible networks below.
   *     'tags[]' => array(), // An array of tag ids.
   *     'media[]' => array(), // An array of these options: 'image', 'text', 'video', 'html'.
   *     'terms[]' => array(), // An array of term ids.
   *     'geo_data_only' => TRUE, // Include geo data only.
   *     'display_name' => string, // Default: 'Discovery'
   *     'display_id' => string, // Default: 'category'
   *     'display_type' => string, // Default: 'dropdown'
   *     'filter_by_claimed' => string,
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function updateFilter($filter_id, $stack, $settings) {
    if (!is_numeric($filter_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla filter id.'), 'error');
      return FALSE;
    }

    $service_path = '/filters/' . $filter_id;
    $response = $this->SubmitRequest($service_path, $stack, 'PUT', $settings);
    $success = (!empty($response) && empty($response->errors)) ? TRUE : FALSE;

    return $success;
  }

  /**
   * Deletes a filter based on the filter id provided.
   *
   * @param int $filter_id
   *   The id of the filter to be deleted.
   *
   * @param string $stack
   *   The stack short name.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function deleteFilter($filter_id, $stack) {
    if (!is_numeric($filter_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla filter id.'), 'error');
      return array();
    }

    $service_path = '/filters/' . $filter_id;
    $response = $this->SubmitRequest($service_path, $stack, 'DELETE', array());
    $success = (!empty($response) && empty($response->errors)) ? TRUE : FALSE;

    return $success;
  }
  //endregion.

  //region Tags.
  /**
   * Gathers all tags related to the stack. Allows
   * filtering by the types.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $types
   *   An array of tag types to filter by.
   *   Possible options: content, product, competition,
   *   system.
   *
   * @return array
   *   Returns an array of the tags associated with the stack,
   *   otherwise returns an empty array.
   */
  public function getAlltags($stack, $types = array()) {
    $data = array();
    if (!empty($types)) {
      $data['type'] = $types;
    }
    $service_path = '/tags';
    $response = $this->SubmitRequest($service_path, $stack, 'GET', $data);
    $response = $this->ParseServiceResponse($response, $service_path, $data);

    if (!empty($response['data'])) {
      return $response['data'];
    }
    else {
      return array();
    }
  }

  /**
   * Gathers information about a specific tag.
   *
   * @param int $tag_id
   *   The tag id.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @return array
   *   Returns an array of information about the tag if
   *   successful, otherwise returns an empty array.
   */
  public function getTag($tag_id, $stack) {
    if (!is_numeric($tag_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla tag id.'), 'error');
      return array();
    }

    $service_path = '/tags/' . $tag_id;
    $response = $this->SubmitRequest($service_path, $stack, 'GET', array());
    $response = $this->ParseServiceResponse($response, $service_path, array());

    if (!empty($response['data'])) {
      return $response['data'];
    }
    else {
      return array();
    }
  }

  /**
   * Creates a new tag with optional settings.
   *
   * @param string $tag
   *   Name for the new tag.
   *
   * @param string $slug
   *   The simplified tag url.
   *
   * @param string $type
   *   The type of tag.
   *   Possible options: content, product, competition,
   *   system.
   *
   * @param bool $enabled
   *   Determines whether or not the tag is enabled.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'vote_enabled' => int, // 1 enabled, 0 disabled.
   *     'target' => string, // Either '_blank', '_self', '_parent' or '_top'.
   *     'price' => string, // Price for tags of type 'product'.
   *     'ext_product_id' => string, // Reference to external products for tags of type 'product'.
   *     'description' => string, // Description for tags of type 'product'.
   *     'image_small_url' => string, // URL of the small image to be displayed.
   *     'image_small_width' => int, // Width of the small image in pixels.
   *     'image_small_height' => int, // Height of the small image in pixels.
   *     'image_medium_url' => string, // URL of the medium image to be displayed.
   *     'image_medium_width' => int, // Width of the medium image in pixels.
   *     'image_medium_height' => int, // Height of the medium image in pixels.
   *   );
   *
   * @return int|bool
   *   Returns the tag id if successful, otherwise FALSE.
   */
  public function createTag($tag, $slug, $type, $enabled, $stack, $settings = array()) {
    $data = array(
      'tag' => $tag,
      'slug' => $slug,
      'type' => $type,
      'enabled' => ($enabled) ? 1 : 0,
    );

    $data += $settings;

    $service_path = '/tags';
    $response = $this->SubmitRequest($service_path, $stack, 'POST', $data);
    $response = $this->ParseServiceResponse($response, $service_path, $data);

    $success = (!empty($response['data']['id'])) ? $response['data']['id'] : FALSE;
    return $success;
  }

  /**
   * Update an existing tag with the settings. Utilizes
   * the PUT http request method.
   *
   * @param int $tag_id
   *   The tag id.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'tag' => string, // Name for the tag.
   *     'slug' => string, // Simplified tag url.
   *     'type' => string,
   *     'vote_enabled' => int, // 1 enabled, 0 disabled.
   *     'target' => string, // Either '_blank', '_self', '_parent' or '_top'.
   *     'price' => string, // Price for tags of type 'product'.
   *     'ext_product_id' => string, // Reference to external products for tags of type 'product'.
   *     'description' => string, // Description for tags of type 'product'.
   *     'image_small_url' => string, // URL of the small image to be displayed.
   *     'image_small_width' => int, // Width of the small image in pixels.
   *     'image_small_height' => int, // Height of the small image in pixels.
   *     'image_medium_url' => string, // URL of the medium image to be displayed.
   *     'image_medium_width' => int, // Width of the medium image in pixels.
   *     'image_medium_height' => int, // Height of the medium image in pixels.
   *   );
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function updateTag($tag_id, $stack, $settings) {
    if (!is_numeric($tag_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla tag id.'), 'error');
      return FALSE;
    }

    $service_path = '/tags/' . $tag_id;
    $response = $this->SubmitRequest($service_path, $stack, 'PUT', $settings);
    $success = (!empty($response) && empty($response->errors)) ? TRUE : FALSE;

    return $success;
  }

  /**
   * Deletes a tag based on the tag id provided.
   *
   * @param int $tag_id
   *   The tag id of the tag to be deleted.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function deleteTag($tag_id, $stack) {
    if (!is_numeric($tag_id)) {
      drupal_set_message(t('A non numeric value was passed as a Stackla filter id.'), 'error');
      return array();
    }

    $service_path = '/tags/' . $tag_id;
    $response = $this->SubmitRequest($service_path, $stack, 'DELETE', array());
    $success = (!empty($response) && empty($response->errors)) ? TRUE : FALSE;

    return $success;
  }
  //endregion.

  /**
   * Submits a request to the Stackla web service.
   *
   * @param string $path
   *   The path to the web service method being called.
   *
   * @param string $stack
   *   The stack shortname this API call applies to.
   *
   * @param string $method
   *   The HTTP request method. Either 'GET', 'POST', 'PUT' or 'DELETE'.
   *
   * @param array $data
   *   An array of name/value pairs that make up the body of the request.
   *
   * @return object
   *   Returns the HTTP response object for the request.
   *
   * @throws StacklaAccessException
   *   Occurs if the user account submitting the request does not have
   *   the correct permissions.
   */
  protected function SubmitRequest($path, $stack, $method = 'GET', $data = array()) {
    if (!user_access(STACKLA_PERM_USE_STACKLA_SERVICE)) {
      throw new StacklaAccessException();
    }
    $request = array();
    $credentials = array(
      'api_key' => $this->serviceApiKey,
      'stack' => $stack,
    );

    // Build the request.
    $url = $this->serviceBaseUrl . $path . '?';

    if (!empty($data) && $method == 'POST') {
      // Input the data query.
      $request = $data;

      $query = http_build_query($credentials);
      $url .= $query;
    }
    elseif (!empty($data) && ($method == 'GET' || $method == 'PUT')) {
      // Build the GET query.
      $data += $credentials;
      $query = http_build_query($data);
      $url .= $query;
    }
    else {
      // No parameters, just build out the authentication.
      $query = http_build_query($credentials);
      $url .= $query;
    }

    $options = array(
      'headers' => array(
        'Content-type' => 'application/x-www-form-urlencoded',
      ),
      'method' => $method,
      'max_redirects' => 1,
      'timeout' => 30,
      'data' => http_build_query($request),
    );
    $response = drupal_http_request($url, $options);

    return $response;
  }

  /**
   * Parses the HTTP response from the web service and returns the results.
   *
   * @param object $response
   *   The HTTP response object.
   *
   * @param string $request
   *   The web service method requested.
   *
   * @param array $data
   *   An array of additional data passed with the request.
   *
   * @return array
   *   Returns an associative array containing the data response from the
   *   service call, otherwise an empty array.
   */
  protected function ParseServiceResponse($response, $request, $data = array()) {
    if ($response && $response->code == '200') {
      $extracted_data = drupal_json_decode($response->data);
    }
    else {
      $message = "
        Stackla Service request failed<br />\n
        Request: @request<br />\n
        Data:<br />\n
        <pre>@data</pre><br />\n
        Response:<br />\n
        <pre>@response</pre>
      ";
      $args = array(
        '@request' => $request,
        '@data' => print_r($data, TRUE),
        '@response' => print_r($response, TRUE),
      );
      watchdog(STACKLA_WATCHDOG, $message, $args, WATCHDOG_ERROR);
      $extracted_data = array();
    }

    return $extracted_data;
  }
}
