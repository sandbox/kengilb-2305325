<?php
/**
 * @file
 * Interface for the Stackla service API calls.
 */

interface iStacklaService {

  //region Filters.
  /**
   * Gathers all filter data related to
   * the passed in stack.
   *
   * @param string $stack
   *   The stack short name to retrieve
   *   the filters for.
   *
   * @return array
   *   Returns an array of filters for
   *   the specified stack.
   */
  public function getAllfilters($stack);

  /**
   * Gathers configuration settings about a filter
   * based on filter id. If there are external
   * products it will gather content that matches all
   * of the tags for this External Product Id.
   *
   * @param int $filter_id
   *   The id of the filter to retrieve content for.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $external_product_ids
   *   An array of external product ids.
   *   i.e. twitter id, facebook id, etc.
   *
   * @param array $external_product_types
   *   An array of external product types.
   *   i.e. twitter, facebook, etc.
   *
   * @return array
   *   Returns an array of information about the filter if
   *   successful, otherwise returns an empty array.
   */
  public function getFilter($filter_id, $stack, $external_product_ids, $external_product_types);

  /**
   * Gathers content that applies to a filter.
   *
   * @param int $filter_id
   *   The id of hte filter to retrieve content for.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'page' => int, // Page.
   *     'limit' => int, // Limit.
   *     'network' => string, // See the possible networks below.
   *     'scope' => string, // Either 'source', 'stackla', or 'all'.
   *     'm2v' => int, // Media type conversion.
   *     'search' => string, // Text search on the stack.
   *     'ext_product_ids' => array(), // An array of external product ids.
   *     'ext_product_types => array(), // An array of external products. i.e. 'instagram'.
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return array
   *   Returns an array of content if successful, otherwise an
   *   empty array.
   */
  public function getFilterContent($filter_id, $stack, $settings);

  /**
   * Creates a new filter with optional settings.
   *
   * @param string $name
   *   The name of the new filter.
   *
   * @param string $stack
   *   The stack shortname to apply the filter to.
   *
   * @param array $settings
   *   array(
   *     'enabled' => 1, // 1, 2 or 3
   *     'system_filter' => 1, // Enable the system filter. 0 for disable.
   *     'enabled_secondary' => 1, // Enable the secondary filter. 0 for disable.
   *     'sort' => 'score_desc', // Either 'score_desc','source_created_at_desc','votes_desc','random'.
   *     'networks' => array(), // An array of available networks, see possible networks below.
   *     'tags' => array(), // An array of tag ids.
   *     'media' => array(), // An array of these options: 'image', 'text', 'video', 'html'.
   *     'terms' => array(), // An array of term ids.
   *     'geo_data_only' => 1, // Include geo data only. 0 for exclude.
   *     'display_name' => string, // Default: 'Discovery'
   *     'display_id' => string, // Default: 'category'
   *     'display_type' => string, // Default: 'dropdown'
   *     'filter_by_claimed' => string,
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function createFilter($name, $stack, $settings);

  /**
   * Updates an existing filter with the given settings.
   * Utilizes the PUT http request method.
   *
   * @param int $filter_id
   *   The Stackla id of the filter to modify.
   *
   * @param string $stack
   *   The stack name.
   *
   * @param array $settings
   *   array(
   *     'name' => string, // New name for the filter.
   *     'enabled' => 1, // 1, 2 or 3
   *     'system_filter' => TRUE, // Enable the system filter.
   *     'enabled_secondary' => TRUE, // Enable the secondary filter.
   *     'sort' => 'score_desc', // Either 'score_desc','source_created_at_desc','votes_desc','random'.
   *     'networks[]' => array(), // An array of available networks, see possible networks below.
   *     'tags[]' => array(), // An array of tag ids.
   *     'media[]' => array(), // An array of these options: 'image', 'text', 'video', 'html'.
   *     'terms[]' => array(), // An array of term ids.
   *     'geo_data_only' => TRUE, // Include geo data only.
   *     'display_name' => string, // Default: 'Discovery'
   *     'display_id' => string, // Default: 'category'
   *     'display_type' => string, // Default: 'dropdown'
   *     'filter_by_claimed' => string,
   *   );
   *   Possible Networks: 'facebook', 'instagram', 'flickr',
   *     'pinterest', 'ecal', 'gplus', 'rss', 'stackla',
   *     'stackla_internal', 'sta_feed', 'tumblr', 'youtube', 'weibo'.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function updateFilter($filter_id, $stack, $settings);

  /**
   * Deletes a filter based on the filter id provided.
   *
   * @param int $filter_id
   *   The id of the filter to be deleted.
   *
   * @param string $stack
   *   The stack short name.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function deleteFilter($filter_id, $stack);
  //endregion.
  //region Tags.
  /**
   * Gathers all tags related to the stack. Allows
   * filtering by the types.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $types
   *   An array of tag types to filter by.
   *   Possible options: content, product, competition,
   *   system.
   *
   * @return array
   *   Returns an array of the tags associated with the stack,
   *   otherwise returns an empty array.
   */
  public function getAlltags($stack, $types);

  /**
   * Gathers information about a specific tag.
   *
   * @param int $tag_id
   *   The tag id.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @return array
   *   Returns an array of information about the tag if
   *   successful, otherwise return an empty array.
   */
  public function getTag($tag_id, $stack);

  /**
   * Creates a new tag with optional settings.
   *
   * @param string $tag
   *   Name for the new tag.
   *
   * @param string $slug
   *   The simplified tag url.
   *
   * @param string $type
   *   The type of tag.
   *   Possible options: content, product, competition,
   *   system.
   *
   * @param bool $enabled
   *   Determines whether or not the tag is enabled.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'vote_enabled' => int, // 1 enabled, 0 disabled.
   *     'target' => string, // Either '_blank', '_self', '_parent' or '_top'.
   *     'price' => string, // Price for tags of type 'product'.
   *     'ext_product_id' => string, // Reference to external products for tags of type 'product'.
   *     'description' => string, // Description for tags of type 'product'.
   *     'image_small_url' => string, // URL of the small image to be displayed.
   *     'image_small_width' => int, // Width of the small image in pixels.
   *     'image_small_height' => int, // Height of the small image in pixels.
   *     'image_medium_url' => string, // URL of the medium image to be displayed.
   *     'image_medium_width' => int, // Width of the medium image in pixels.
   *     'image_medium_height' => int, // Height of the medium image in pixels.
   *   );
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function createTag($tag, $slug, $type, $enabled, $stack, $settings);

  /**
   * Updated an existing tag with the settings. Utilizes
   * the PUT http request method.
   *
   * @param int $tag_id
   *   The tag id.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @param array $settings
   *   array(
   *     'vote_enabled' => int, // 1 enabled, 0 disabled.
   *     'target' => string, // Either '_blank', '_self', '_parent' or '_top'.
   *     'price' => string, // Price for tags of type 'product'.
   *     'ext_product_id' => string, // Reference to external products for tags of type 'product'.
   *     'description' => string, // Description for tags of type 'product'.
   *     'image_small_url' => string, // URL of the small image to be displayed.
   *     'image_small_width' => int, // Width of the small image in pixels.
   *     'image_small_height' => int, // Height of the small image in pixels.
   *     'image_medium_url' => string, // URL of the medium image to be displayed.
   *     'image_medium_width' => int, // Width of the medium image in pixels.
   *     'image_medium_height' => int, // Height of the medium image in pixels.
   *   );
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function updateTag($tag_id, $stack, $settings);

  /**
   * Deletes a tag based on the tag id provided.
   *
   * @param int $tag_id
   *   The tag id of the tag to be deleted.
   *
   * @param string $stack
   *   The stack shortname.
   *
   * @return bool
   *   Returns TRUE if successful, otherwise FALSE.
   */
  public function deleteTag($tag_id, $stack);
  //endregion.
}
