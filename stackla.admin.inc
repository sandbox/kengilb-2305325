<?php
/**
 * @file
 * Administrative settings for the Stackla module.
 */

/**
 * Generates the Stackla settings form.
 *
 * @return array
 *   A form array for the settings form.
 */
function stackla_settings_form() {
  $form = array();

  $form['intro'] = array(
    '#prefix' => '<div class="stackla-intro">',
    '#suffix' => '</div>',
    '#markup' => t('This screen allows you to configure the Stackla service connection settings.'),
  );

  $form['stackla_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Enter the API key provided by Stackla.'),
    '#default_value' => variable_get('stackla_api_key', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
